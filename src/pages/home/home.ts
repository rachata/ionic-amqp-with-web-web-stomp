import { Component, OnInit } from '@angular/core';
import { NavController } from 'ionic-angular';
import * as Amqp from "amqp-ts";
import { RxStompService } from '@stomp/ng2-stompjs';
import { Message } from '@stomp/stompjs';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage implements OnInit {

  constructor(public navCtrl: NavController , private rxStompService: RxStompService) {



  }

  ngOnInit(){

  this.rxStompService
    .watch('/exchange/riders/001')
    .subscribe((message: Message) => {

      console.log(JSON.stringify(message.body))
      
    });


  }

}
