import { InjectableRxStompConfig } from '@stomp/ng2-stompjs';

export const myRxStompConfig: InjectableRxStompConfig = {
  // Which server?
  brokerURL: 'ws://139.59.118.178:15674/ws',

  
  connectHeaders: {
    login: 'test',
    passcode: 'test',
  },

  
  heartbeatIncoming: 0, 
  heartbeatOutgoing: 20000, 


  reconnectDelay: 200,

  debug: (msg: string): void => {
    console.log(new Date(), msg);
  },
};